﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Helpers
{
    public class Sha2
    {

        public static string getStringSHA(string cadena)
        {
            var enc = Encoding.UTF8;
            using (var sha2 = new SHA512CryptoServiceProvider())
            {
                byte[] data = enc.GetBytes(cadena);
                byte[] hash = sha2.ComputeHash(data);
                StringBuilder sb = new StringBuilder(hash.Length * 2);
                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("x2"));
                }
                return sb.ToString();
            }
        }
    }
}
