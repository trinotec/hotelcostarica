﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HotelCostaRicaCore.Web.Helpers;
using Microsoft.Extensions.Configuration;
using System.IO;
using Newtonsoft.Json;
using HotelCostaRicaCore.Web.Models;
using HotelCostaRicaCore.Web.Services;

namespace HotelCostaRicaCore.Web.Controllers
{
    

    public class NosotrosController : Controller
    {

        private readonly IEmailSender _emailSender;
        private readonly IApiService _apiService;

        IConfigurationRoot Configuration { get; } =
        ConfigurationHelper.GetConfiguration(Directory.GetCurrentDirectory());

        public NosotrosController(IEmailSender emailSender, IApiService apiService)
        {
            _emailSender = emailSender;
            this._apiService = apiService;
        }


        public async Task<IActionResult> Index()
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //Informacion Blogs ------------------------------
            var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
            if (responseBlog.Result != null && responseBlog.Result.blog.Count > 0)
            {
                ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);
            }

            return View();
        }
        public async Task<IActionResult> Blog(int id = 1)
        {
            //id es el numero de pagina
            int ArtculosPorPagina = 3;
            var reader = new StreamReader(Directory.GetCurrentDirectory() + "/appsettings.json");
            var appSettings = JsonConvert.DeserializeObject<AppSettingsProy>(reader.ReadToEnd());
            ViewBag.Articulos = appSettings.Articulos;
            ViewBag.MasArticulos = appSettings.Articulos.Take(3).ToList();


            ViewBag.totalblogs = 0;
            ViewBag.NumPagina = (id < 0 ) ? 1 : id;
            ViewBag.blogsPorPagina = ArtculosPorPagina;


            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //Informacion Blogs ------------------------------
            var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
            if (responseBlog.IsSuccess)
            {
                if (responseBlog.Result.blog.Count > 0)
                {
                    ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);
                    ViewBag.totalblogs = responseBlog.Result.blog.Count;
                    int skipBlog = (id * ArtculosPorPagina - ArtculosPorPagina);
                    ViewBag.Blogs = responseBlog.Result.blog.Skip(skipBlog).Take(ArtculosPorPagina);
                }
            }


            //--------------------------------------           
            ViewBag.ArticulosFooter = appSettings.Articulos.Take(3);

            return View();
        }

        public async Task<IActionResult> FAQ()
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //Informacion Blogs ------------------------------
            var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
            if (responseBlog.Result != null && responseBlog.Result.blog.Count > 0)
            {
                ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);
            }

            return View();
        }

        public async Task<IActionResult> Contacto()
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //Informacion Blogs ------------------------------
            var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
            if (responseBlog.Result != null && responseBlog.Result.blog.Count > 0)
            {
                ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);
            }

            return View();
        }

        public async Task<IActionResult> InternaBlog(int Id)
        {
 
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //Informacion Blogs ------------------------------
            var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
            if (responseBlog.Result != null && responseBlog.Result.blog.Count > 0)
            {
                ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);
                ViewBag.MasArticulos = responseBlog.Result.blog.Skip(0).Take(3);
            }

            ViewBag.IdBlog = Id;
            ViewBag.totalComentariosInicial = 0;

            //Informacion Blog detalle ------------------------------
            var controller = "/api/Blog/Consultar?idBlog=" + Id;
            var responseBlogdetalle = await _apiService.GetBlogDetalle(url, Constants.UrlAPP, controller, _dataAccess.token_type, _dataAccess.access_token);
            if (responseBlogdetalle.IsSuccess)
            {
                ViewBag.blog = responseBlogdetalle.Result.blog;
                ViewBag.bloggers = responseBlogdetalle.Result.Bloggers;
                ViewBag.totalComentariosInicial = responseBlogdetalle.Result.Bloggers.Count;

                if (responseBlog.Result.blog.Count > 0)
                {
                    //int index = myList.FindIndex(a => a.Prop == oProp);
                    int indexBlog = responseBlog.Result.blog.FindIndex(x => x.idBlog == Id);
                                        
                    if(indexBlog == 0)
                    {
                        if(responseBlog.Result.blog.Count == 1)
                        {
                            ViewBag.BlogAnterior = responseBlog.Result.blog[0];
                        }
                        else
                        {
                            ViewBag.BlogAnterior = responseBlog.Result.blog[responseBlog.Result.blog.Count - 1];
                        }
                    }
                    else
                    {
                        ViewBag.BlogAnterior = responseBlog.Result.blog[indexBlog - 1];
                    }
                    

                    
                    if (indexBlog == (responseBlog.Result.blog.Count -1) )
                    {
                        ViewBag.BlogSiguiente = (responseBlog.Result.blog[0]);                        
                    }
                    else
                    {
                        ViewBag.BlogSiguiente = (responseBlog.Result.blog[indexBlog + 1]);
                    }

                }

            }

            return View();
        }

        public async Task<IActionResult> EnviarCorreo(MensajeContacto data)
        {
            
            string Mensaje = "<b>Nombre: </b>" + data.NomCliente + "<br>";
            Mensaje += "<b>Correo Electronico: </b>" + data.Email + "<br>";
            Mensaje += "<b>Mensaje: </b>" + data.Mensaje + "<br>";

            try
            {
                await _emailSender.SendEmailAsync("hulloa@sicsoftsa.com", "Asunto Correo", Mensaje, "mzamora@sicsoftsa.com").ConfigureAwait(false);                               
                return Json(new { IsSuccess = true });
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return Json(new { IsSuccess = false });
            }

            return Json(new { IsSuccess = true });
        }


        [HttpPost]
        public async Task<IActionResult> GuardarComentario(RequestComentarioBlog data)
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;


            //Guardar comentario ------------------------------
            var responseGuardarComentarioBlog = await _apiService.PostGuardarComentarioBlog(url, Constants.UrlAPP, "/api/Blog/InsertarComentario", _dataAccess.token_type, _dataAccess.access_token, data);
            if(responseGuardarComentarioBlog.IsSuccess == true)
            {
                return Json(new { IsSuccess = true, Resultado = responseGuardarComentarioBlog.Result });
            }
            else
            {
                return Json(new { IsSuccess = false, Mensaje = responseGuardarComentarioBlog.Message });
            }
        }


        public async Task<IActionResult> TerminosyCondiciones()
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //Informacion Blogs ------------------------------
            var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
            if (responseBlog.Result != null && responseBlog.Result.blog.Count > 0)
            {
                ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);
                ViewBag.TerminosyCondiciones = responseBlog.Result.legal.TerminosYCondiciones;
            }

            return View();
        }


        public async Task<IActionResult> PoliticasCancelacion()
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //Informacion Blogs ------------------------------
            var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
            if (responseBlog.Result != null && responseBlog.Result.blog.Count > 0)
            {
                ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);
                ViewBag.PoliticasCancelacion = responseBlog.Result.legal.PoliticasCancelacion;
            }

            return View();
        }

        public async Task<IActionResult> PoliticasPrivacidad()
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //Informacion Blogs ------------------------------
            var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
            if (responseBlog.Result != null && responseBlog.Result.blog.Count > 0)
            {
                ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);
                ViewBag.PoliticasPrivacidad = responseBlog.Result.legal.PoliticasPrivacidad;
            }

            return View();
        }

    }

}