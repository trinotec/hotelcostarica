﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HotelCostaRicaCore.Web.Helpers;
using HotelCostaRicaCore.Web.Models;
using HotelCostaRicaCore.Web.Services;
using HotelCostaRicaCore.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HotelCostaRicaCore.Web.Controllers
{
    public class HotelesController : Controller
    {
        
        private readonly IApiService _apiService;


        public HotelesController( IApiService apiService)
        {            
            this._apiService = apiService;
        }

        //[HttpPost]
        public async Task<IActionResult> Index( int adultos=1, int ninos=0, string fechasBusqueda ="", string destino ="", int habitaciones = 1, string provincia=null)
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;
            ViewBag.adultos = adultos;
            ViewBag.ninos = ninos;
            ViewBag.Destino = destino;
            ViewBag.habitaciones = habitaciones;
            ViewBag.Provincia =(string.IsNullOrEmpty(provincia))?"Todas las provincias": provincia;
                       

            ViewBag.fechasBusquedaInicial = fechasBusqueda;
            if (fechasBusqueda != "")
            {
                try
                {
                    var fechasArray = fechasBusqueda.Split("-");
                    var fecha1 = fechasArray[0].Trim();
                    var fechaDivida1 = fecha1.Split("/");

                    var fecha2 = fechasArray[1].Trim();
                    var fechaDivida2 = fecha2.Split("/");

                    ViewBag.fechasBusquedaIniMostrar = fechaDivida1[1] + "/" + fechaDivida1[0] + "/" + fechaDivida1[2] + " - " + fechaDivida2[1] + "/" + fechaDivida2[0] + "/" + fechaDivida2[2];                    
                }
                catch
                {
                    ViewBag.fechasBusquedaIniMostrar = "";
                    
                }
                
            }
            else
            {
                ViewBag.fechasBusquedaIniMostrar = "";                
            }


            //Informacion Blogs ------------------------------
            var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
            if (responseBlog.Result != null && responseBlog.Result.blog.Count > 0)
            {
                ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);
            }

            return View();
        }

        public async Task<IActionResult> Single(string id, int totalRooms=0, int totalAdult=0, int totalChild=0 , string fechaInicio=null, string fechaFin=null)
        {
             
            try
            {

                //informacion de acceso ------------------------------
                var url = Constants.UrlAPI;
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;

                string controlApi = "/api/Hoteles/" + id;
                //Informacion Hotel detalle ------------------------------
                var responseHotel = await _apiService.GetHotelDetalle(url, Constants.UrlAPP, controlApi, _dataAccess.token_type, _dataAccess.access_token);
                if (responseHotel.IsSuccess)
                {
                    ViewBag.Hotel = responseHotel.Result;
                    
                    if (responseHotel.Result.PalabrasClaves != null)
                    {
                        ViewBag.Tags = responseHotel.Result.PalabrasClaves.Split(";").Where(x => x != "").ToList();
                    }
                    else
                    {
                        ViewBag.Tags = null;
                    }

                }

                //Fotos Hoteles  ------------------------------               
                string controllerHabitacionFotos = "/api/HotelesFotos/" + id;
                var responseFotoHoteles = await _apiService.GetHotelesFotos(url, Constants.UrlAPP, controllerHabitacionFotos, _dataAccess.token_type, _dataAccess.access_token);
                if (responseFotoHoteles.IsSuccess)
                {
                    ViewBag.FotosHotel1 = responseFotoHoteles.Result.Where(x => x.CodHotel == id).Skip(0).Take(2).ToList();
                    ViewBag.FotosHotel2Grande = responseFotoHoteles.Result.Where(x => x.CodHotel == id).Skip(2).Take(1).FirstOrDefault();
                    ViewBag.FotosHotel3 = responseFotoHoteles.Result.Where(x => x.CodHotel == id).Skip(3).Take(2).ToList();                    
                    ViewBag.FotosHotel4 = responseFotoHoteles.Result.Where(x => x.CodHotel == id).Skip(5).Take(1).FirstOrDefault();                    
                    var fotosEscondidas = responseFotoHoteles.Result.Where(x => x.CodHotel == id).Skip(6).ToList();
                    ViewBag.FotosHotelHide = cadenaFotosGaleria(fotosEscondidas);
                    ViewBag.FotosHotelHideContador = fotosEscondidas.Count();
                }
                else
                {
                    ViewBag.FotosHotelHide = "";
                }


                //Habitaciones ------------------------------   
                Response<List<Habitacion>> responseHabitaciones;
                if (fechaInicio != null && fechaInicio != null)
                {
                    var fechaInicioArray = fechaInicio.Split("/");
                    var fechaFinArray = fechaFin.Split("/");

                    HabitacionRequest habRequest = new HabitacionRequest
                    {
                        codHotel = id,
                        cantAdultos = totalAdult,
                        cantNinos = totalChild,
                        fechaInicio = fechaInicioArray[2] + "-" + fechaInicioArray[0] + "-" + fechaInicioArray[1],
                        fechaFinal = fechaFinArray[2] + "-" + fechaFinArray[0] + "-" + fechaFinArray[1]
                    };
                    responseHabitaciones = await _apiService.GetHabitacionesDisponibles(url, Constants.UrlAPP, "/api/CCHabitacionesTipos/GetDisponibilidad", _dataAccess.token_type, _dataAccess.access_token, habRequest);
                }
                else
                {
                    string controllerHab = "/api/CCHabitacionesTipos/"+ id;
                    responseHabitaciones = await _apiService.GetHabitacionesHotel(url, Constants.UrlAPP, controllerHab, _dataAccess.token_type, _dataAccess.access_token);
                }

                

                if (responseHabitaciones.IsSuccess)
                {
                    foreach(var habitacion in responseHabitaciones.Result)
                    {
                        HabitacionFotoRequest HabFotoReq = new HabitacionFotoRequest() {
                            CodHotel = id,
                            CodTipoHab = habitacion.CodTipoHab
                        };

                        var ResultadofotoHabitacion = await _apiService.GetHabitacionFotos(url, Constants.UrlAPP, "/api/CCHabitacionesFotos", _dataAccess.token_type, _dataAccess.access_token, HabFotoReq);

                        if(ResultadofotoHabitacion.IsSuccess == true && ResultadofotoHabitacion.Result.Count > 0 )
                        {
                            habitacion.Fotos = ResultadofotoHabitacion.Result;
                            habitacion.FotosCadena = cadenaFotosHabitacion(ResultadofotoHabitacion.Result);
                            habitacion.FotoPrincipal = habitacion.Fotos[0].Imagen;
                        }
                        else
                        {
                            habitacion.Fotos = new List<HabitacionFoto>();
                            habitacion.FotosCadena = "";
                            habitacion.FotoPrincipal = "";
                        }
                    }


                    ViewBag.Habitaciones = responseHabitaciones.Result;
                }
                else
                {
                    ViewBag.Habitaciones = new List<Habitacion>();
                }

                //Testimonial ------------------------------   
                string controllerTestimonialHotel = "/api/HotelesTestimonial/GetFullInfo/"+ id;
                var responseTestimonialHotel = await _apiService.GetCalificacionHotel(url, Constants.UrlAPP, controllerTestimonialHotel, _dataAccess.token_type, _dataAccess.access_token);
                if (responseTestimonialHotel.IsSuccess)
                {
                    ViewBag.TestimonialHotel = responseTestimonialHotel.Result;
                    double sumaCalificacion = responseTestimonialHotel.Result.Comfort + responseTestimonialHotel.Result.Limpieza + responseTestimonialHotel.Result.Staf + responseTestimonialHotel.Result.Facilidades;
                    ViewBag.PromedioCalificacion = (sumaCalificacion > 0) ? sumaCalificacion / 4 : 0;
                }
                else
                {
                    ViewBag.TestimonialHotel = new CalificacionHotel();
                    ViewBag.PromedioCalificacion = 0;
                }

                //Informacion Blogs ------------------------------
                var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
                if (responseBlog.Result != null && responseBlog.Result.blog.Count > 0)
                {
                    ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);
                }

            }
            catch (Exception ex)
            {

            }

            //--------------------------------------
            var reader = new StreamReader(Directory.GetCurrentDirectory() + "/appsettings.json");
            var appSettings = JsonConvert.DeserializeObject<AppSettingsProy>(reader.ReadToEnd());
            ViewBag.ArticulosFooter = appSettings.Articulos.Take(3);

            return View();
        }

        public string cadenaFotosGaleria(IEnumerable<HotelFoto> Fotos)
        {
            string Cadena = "";

            if(Fotos.Count() > 0)
            {
                foreach (var foto in Fotos)
                {
                    Cadena = Cadena +  "{'src': '" + foto.UrlFoto + "'},";
                }
                Cadena = Cadena.Substring(0, (Cadena.Count() - 1));
            }

            return Cadena;
        }

        public string cadenaFotosHabitacion(IEnumerable<HabitacionFoto> Fotos)
        {
            string Cadena = "";

            if (Fotos.Count() > 0)
            {
                foreach (var foto in Fotos)
                {
                    Cadena = Cadena + "{'src': '" + foto.Imagen + "'},";
                }
                Cadena = Cadena.Substring(0, (Cadena.Count() - 1));
            }

            return Cadena;
        }

        public async Task<IActionResult> Habitacion(string codHotel, string codHabitacion)
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //Habitaciones ------------------------------     
            var responseHabitacion = await _apiService.GetHabitacionDetalle(url, Constants.UrlAPP, "/api/CCHabitacionesTipos", _dataAccess.token_type, _dataAccess.access_token, codHotel, codHabitacion);
            if (responseHabitacion.IsSuccess)
            {
                ViewBag.Habitacion = responseHabitacion.Result[0];

            }
            else
            {
                ViewBag.Habitacion = new Habitacion();
            }

            //Fotos Habitacion
            HabitacionFotoRequest HabFotoReq = new HabitacionFotoRequest()
            {
                CodHotel = codHotel,
                CodTipoHab = codHabitacion
            };

            var ResultadofotoHabitacion = await _apiService.GetHabitacionFotos(url, Constants.UrlAPP, "/api/CCHabitacionesFotos", _dataAccess.token_type, _dataAccess.access_token, HabFotoReq);

            if (ResultadofotoHabitacion.IsSuccess == true && ResultadofotoHabitacion.Result.Count > 0)
            {
                ViewBag.TotalFotos= ResultadofotoHabitacion.Result.Count;
                ViewBag.FotosCadena = cadenaFotosHabitacion(ResultadofotoHabitacion.Result);
                ViewBag.FotoHeader = ResultadofotoHabitacion.Result[0].Imagen; //ResultadofotoHabitacion.Result[0].Imagen;//habitacion.Fotos[0].Imagen;
            }
            else
            {
                ViewBag.FotosCadena = "";
                ViewBag.FotoHeader = "";
                ViewBag.TotalFotos = 0;
            }

            //--------------------------------------
            var reader = new StreamReader(Directory.GetCurrentDirectory() + "/appsettings.json");
            var appSettings = JsonConvert.DeserializeObject<AppSettingsProy>(reader.ReadToEnd());
            ViewBag.ArticulosFooter = appSettings.Articulos.Take(3);


            //return View();
            return await Task.Run(() => View());
        }

        [HttpPost]
        public async Task<IActionResult> BusquedaHoteles(ParametrosFiltro parametro)
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            if(string.IsNullOrEmpty(parametro.FechaInicio))
            {   
                var responseHotelesTotales = await _apiService.GetHoteles(url, Constants.UrlAPP, "/api/Hoteles", _dataAccess.token_type, _dataAccess.access_token);
                if (responseHotelesTotales.IsSuccess)
                {
                    int HotelesEncontrados = 0;                    
                    if (responseHotelesTotales.Result != null && responseHotelesTotales.Result.Count > 0)
                    {
                        HotelesEncontrados = responseHotelesTotales.Result.Count;                        
                    }

                    return Json(new { IsSuccess = true, Hoteles = responseHotelesTotales.Result, HotelesEncontrados = HotelesEncontrados });
                }
                else
                {
                    return Json(new { IsSuccess = false });
                }
            }

            //informacion de hoteles            
            string Parametros = ConstructorParmetroURL(parametro);
            var responseHoteles = await _apiService.GetHotelesFiltro(url, Constants.UrlAPP, "/api/Hoteles/Filtro", _dataAccess.token_type, _dataAccess.access_token, Parametros);            
            if (responseHoteles.IsSuccess)
            {
                int HotelesEncontrados = 0;
                var HotelLista = new List<Hotel>();

                if(parametro.CheckEstrella3 == false || parametro.CheckEstrella4 == false || parametro.CheckEstrella5 == false)
                {
                    HotelLista = FiltroEstrellas(responseHoteles.Result, parametro);
                }
                else
                {
                    HotelLista = responseHoteles.Result;
                }
 


                if (HotelLista != null && HotelLista.Count> 0)
                {
                    HotelesEncontrados = responseHoteles.Result.Count;

                    //0 popularidad, 1 puntuacion media., 2 precio mas alto a mas bajo, 3 precio mas bajo al mas alto.
                    if (parametro.Orden == 0)
                    {
                        HotelLista = HotelLista.OrderByDescending(x => x.Popular).ToList(); ;
                    }
                    else if (parametro.Orden == 1)
                    {
                        HotelLista = HotelLista.OrderByDescending(x => x.Calificacion).ToList(); ;
                    }
                    else if (parametro.Orden == 2)
                    {
                        HotelLista = HotelLista.OrderBy(x => x.PrecioPromedio).ToList(); 
                    }
                    else
                    {
                        HotelLista = HotelLista.OrderByDescending(x => x.PrecioPromedio).ToList(); ;
                    }

                }

                return Json(new { IsSuccess = true , Hoteles = HotelLista, HotelesEncontrados = HotelesEncontrados });
            }
            else
            {
                return Json(new { IsSuccess = false });
            }       
        }

        public string ConstructorParmetroURL(ParametrosFiltro Parametros)
        {
            string URL = "";

            var ArrayFechaIni = Parametros.FechaInicio.Split("/");
            URL = URL + "?fechaInicio=" + ArrayFechaIni[2] + "-" + ArrayFechaIni[0] + "-" + ArrayFechaIni[1]+"&";

            var ArrayfechaFinal = Parametros.FechaFin.Split("/");
            URL = URL + "fechaFinal=" + ArrayfechaFinal[2] + "-" + ArrayfechaFinal[0] + "-" + ArrayfechaFinal[1] + "&";

            URL = URL + "cantAdultos=" + Parametros.TotalAdultos + "&";
            URL = URL + "cantNinos=" + Parametros.TotalNinos + "&";

            URL = URL + "PrecioInicial=" + Parametros.PrecioMinimo + "&";
            URL = URL + "PrecioFinal=" + Parametros.PrecioMax + "&";

            if(Parametros.CheckAire == true)
            {
                URL += "aire=true&";
            }
            if (Parametros.Checkdesayuno == true)
            {
                URL += "desayuno=true&";
            }
            if (Parametros.Checkpiscina == true)
            {
                URL += "piscina=true&";
            }
            if (Parametros.CheckWifi == true)
            {
                URL += "wifi=true&";
            }
            if (Parametros.CheckParqueo == true)
            {
                URL += "parqueo=true&";
            }
            if (Parametros.CheckDivisas == true)
            {
                URL += "divisas=true&";
            }

            if (Parametros.Provincia != "Todas las provincias")
            {
                URL += "Ciudad="+ Parametros.Provincia + "&";
            }

            if (!string.IsNullOrEmpty(Parametros.Textobusqueda))
            {
                URL += "nombre=" + Parametros.Textobusqueda + "&";
            }


            URL = URL.Substring(0, (URL.Length - 1));
            return URL;
        }


        public List<Hotel> FiltroEstrellas(List<Hotel> Hoteles, ParametrosFiltro Parametros )
        {
            var HotelLista = new List<Hotel>();

            if (Parametros.CheckEstrella3 == true)
            {
                var hotel3E = Hoteles.Where(x => x.Estrellas == 3);
                HotelLista.AddRange(hotel3E);
            }

            if (Parametros.CheckEstrella4 == true)
            {
                var hotel4E = Hoteles.Where(x => x.Estrellas == 4);
                HotelLista.AddRange(hotel4E);
            }

            if (Parametros.CheckEstrella5 == true)
            {
                var hotel5E = Hoteles.Where(x => x.Estrellas == 5);
                HotelLista.AddRange(hotel5E);
            }
            return HotelLista;
        }


    }
}