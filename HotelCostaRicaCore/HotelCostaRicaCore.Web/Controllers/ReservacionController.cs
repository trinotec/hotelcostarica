﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HotelCostaRicaCore.Web.Helpers;
using HotelCostaRicaCore.Web.Models;
using HotelCostaRicaCore.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using System.Text;
using Microsoft.Extensions.Logging;

namespace HotelCostaRicaCore.Web.Controllers
{
    public class ReservacionController : Controller
    {
        private readonly IApiService _apiService;
        private readonly ILogger<ReservacionController> logger;

        private string fraseEncriptacion = "C0st4R1c4Ch3ck1n";
        private string algoritmoEncriptacionHASH = "MD5";
        private string valorRGBSalt = "TextoCualquiera";
        private int iteraciones = 22;
        private string vectorInicial = "1234567891234567";
        private int tamanoClave = 128;


        public ReservacionController(IApiService apiService, ILogger<ReservacionController>  logger)
        {
            this._apiService = apiService;
            this.logger = logger;
        }

        public async Task<IActionResult> Index(string habitacionEscogida, string IdHotel, string fechaInicio, string fechaFin, int qty3, int qty2)
        {

            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //Informacion Blogs ------------------------------
            var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
            if (responseBlog.Result != null && responseBlog.Result.blog.Count > 0)
            {
                ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);
                ViewBag.TipoBanco = responseBlog.Result.legal.TipoBanco;                
            }



            //Habitaciones ------------------------------                 
            var responseHabitacion = await _apiService.GetHabitacionDetalle(url, Constants.UrlAPP, "/api/CCHabitacionesTipos", _dataAccess.token_type, _dataAccess.access_token, IdHotel, habitacionEscogida);
            if (responseHabitacion.IsSuccess)
            {
                var HabitacionSeleccionada = responseHabitacion.Result[0];
                ViewBag.Habitacion = HabitacionSeleccionada;
                ViewBag.PrecioNocheHabitacion = HabitacionSeleccionada.Precio;
            }
            else
            {
                ViewBag.Habitacion = new Habitacion();
            }


            //Informacion Hotel detalle ------------------------------
            string controlApi = "/api/Hoteles/" + IdHotel;
            var responseHotel = await _apiService.GetHotelDetalle(url, Constants.UrlAPP, controlApi, _dataAccess.token_type, _dataAccess.access_token);
            if (responseHotel.IsSuccess)
            {
                ViewBag.Hotel = responseHotel.Result;
            }
            //------------------------------------------------


            DateTime DtFechaini;
            DateTime DtFechaFin;

            var arrayFechaIni = fechaInicio.Split("-");
            var arrayFechaFin = fechaFin.Split("-");
            fechaInicio = arrayFechaIni[2] + "/" + arrayFechaIni[1] + "/" + arrayFechaIni[0];
            fechaFin = arrayFechaFin[2] + "/" + arrayFechaFin[1] + "/" + arrayFechaFin[0];

            ViewBag.HabitacionDisponible = "False";
            ViewBag.ClassHabDisable = "custom-form disableForm";
            double TotalHabitacion = 0;
            ViewBag.TotalHabitacion = TotalHabitacion;
            if (DateTime.TryParse(fechaInicio, out DtFechaini) && DateTime.TryParse(fechaFin, out DtFechaFin))
            {

                ViewBag.FechaInicio = DtFechaini.ToString("dd/MM/yyyy");
                ViewBag.Fin = DtFechaFin.ToString("dd/MM/yyyy");
                ViewBag.DiasTotales = "0";

                ViewBag.CodHotel = IdHotel;
                ViewBag.CodTipoHab = habitacionEscogida;
                ViewBag.FechaIniYMD = DtFechaini.ToString("yyyy-MM-dd");
                ViewBag.FechaFinYMD = DtFechaFin.ToString("yyyy-MM-dd");
                ViewBag.FechaFinYMD = DtFechaFin.ToString("yyyy-MM-dd");


                ViewBag.FechaInicio = DtFechaini;
                ViewBag.Fin = DtFechaFin;
                var resultFechas = DtFechaFin.Subtract(DtFechaini);
                ViewBag.DiasTotales = resultFechas.TotalDays;

                try
                {
                    //Verificacion Disponibilidad
                    HabitacionRequest habRequest = new HabitacionRequest
                    {
                        codHotel = IdHotel,
                        cantAdultos = qty3,
                        cantNinos = qty2,
                        fechaInicio = DtFechaini.ToString("yyyy-MM-dd"),
                        fechaFinal = DtFechaFin.ToString("yyyy-MM-dd")
                    };

                    var responseHabitacionesDisponibles = await _apiService.GetHabitacionesDisponibles(url, Constants.UrlAPP, "/api/CCHabitacionesTipos/GetDisponibilidad", _dataAccess.token_type, _dataAccess.access_token, habRequest);
                    if (responseHabitacionesDisponibles.IsSuccess)
                    {
                        ViewBag.HabitacionDisponible = false;
                        if (responseHabitacionesDisponibles.Result.Count > 0)
                        {
                            var HabDisponible = responseHabitacionesDisponibles.Result.Where(x => x.CodTipoHab == habitacionEscogida).FirstOrDefault();
                            if (HabDisponible != null)
                            {
                                ViewBag.ClassHabDisable = "custom-form";
                                ViewBag.HabitacionDisponible = "True";

                                //DtFechaini DtFechaFin
                                TotalHabitacion = HabDisponible.Precio;
                                ViewBag.TotalHabitacion = TotalHabitacion;
                            }
                        }
                    }
                    else
                    {

                    }
                }
                catch
                {

                }
            }
            else
            {
                ViewBag.FechaInicio = "";
                ViewBag.Fin = "";
                ViewBag.DiasTotales = "";
            }


            ViewBag.Adultos = qty3;
            ViewBag.ninos = qty2;

            ViewBag.acquirerId = Constants.acquirerId;
            ViewBag.idCommerce = Constants.idCommerce;
            ViewBag.purchaseCurrencyCode = Constants.purchaseCurrencyCode;
            ViewBag.language = Constants.language;
            ViewBag.shippingFirstName = Constants.shippingFirstName;
            ViewBag.shippingLastName = Constants.shippingLastName;
            ViewBag.shippingEmail = Constants.shippingEmail;
            ViewBag.shippingAddress = Constants.shippingAddress;
            ViewBag.shippingZIP = Constants.shippingZIP;
            ViewBag.shippingCity = Constants.shippingCity;
            ViewBag.shippingState = Constants.shippingState;
            ViewBag.shippingCountry = Constants.shippingCountry;
            ViewBag.programmingLanguage = Constants.programmingLanguage;
            logger.LogInformation("Testeando:");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SolicitarReservacion(string CodHotel, string CodTipoHab, string FechaReserva, string FechaLlegada, string FechaSalida,
            int NumNoches, double PrecioNoche, string Nombres, string Apellidos, string Telefonos, string NumIdentificacion,
            string CodPais, string CodCiudad, string Email, string Empresa, string Comentarios, int Adultos, int Ninos, double MontoTotalReserva, double MontoPagado, double PagoTarjeta, string CodReserva)
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //informacion de reservacion

            var StringContent = "";

            if (!string.IsNullOrEmpty(CodReserva))
            {
                var RequestReservacion = new ReservacionCodReservaAct
                {
                    Adultos = Adultos,
                    Apellidos = Apellidos,
                    CodCiudad = CodCiudad,
                    CodHotel = CodHotel,
                    CodPais = CodPais,
                    CodTipoHab = CodTipoHab,
                    Comentarios = Comentarios,
                    Email = Email,
                    Empresa = Empresa,
                    FechaLlegada = FechaLlegada,
                    FechaReserva = FechaReserva,
                    FechaSalida = FechaSalida,
                    MontoPagado = 0,
                    MontoTotalReserva = MontoTotalReserva,
                    Ninos = Ninos,
                    Nombres = Nombres,
                    NumIdentificacion = "",
                    NumNoches = NumNoches,
                    PagoTarjeta = 0,
                    PrecioNoche = PrecioNoche,
                    Telefonos = Telefonos,
                    CodReserva = CodReserva
                };

                var requestReservacion = new RequestReservacion()
                {
                    reserva = RequestReservacion,
                    bin = "",
                    last4 = ""
                };
                StringContent = JsonConvert.SerializeObject(requestReservacion);
            }
            else
            {
                var RequestReservacion = new Reservacion
                {
                    Adultos = Adultos,
                    Apellidos = Apellidos,
                    CodCiudad = CodCiudad,
                    CodHotel = CodHotel,
                    CodPais = CodPais,
                    CodTipoHab = CodTipoHab,
                    Comentarios = Comentarios,
                    Email = Email,
                    Empresa = Empresa,
                    FechaLlegada = FechaLlegada,
                    FechaReserva = FechaReserva,
                    FechaSalida = FechaSalida,
                    MontoPagado = 0,
                    MontoTotalReserva = MontoTotalReserva,
                    Ninos = Ninos,
                    Nombres = Nombres,
                    NumIdentificacion = "",
                    NumNoches = NumNoches,
                    PagoTarjeta = 0,
                    PrecioNoche = PrecioNoche,
                    Telefonos = Telefonos
                };

                var requestReservacion = new RequestReservacion()
                {
                    reserva = RequestReservacion,
                    bin = "",
                    last4 = ""
                };
                StringContent = JsonConvert.SerializeObject(requestReservacion);
            }

            /*
            var requestReservacion = new RequestReservacion()
            {
                reserva = RequestReservacion,
                bin = "",
                last4 = ""
            }; */

            try
            {
                var responseReservaPrevia = await _apiService.PostInsertReservation(url, Constants.UrlAPP, "/api/CCReservas/InsertarReserva", _dataAccess.token_type, _dataAccess.access_token, StringContent);
                if (responseReservaPrevia.IsSuccess)
                {
                    var purchaseOperationNumb = responseReservaPrevia.Result.orderReference.ToString("D9");
                    var purchaseAmountString = MontoTotalReserva.ToString("F");
                    purchaseAmountString = purchaseAmountString.Replace(".", "");

                    HttpContext.Session.SetString("purchaseOperationNumb", purchaseOperationNumb);
                    HttpContext.Session.SetString("CodReserva", responseReservaPrevia.Result.CodReserva);
                    HttpContext.Session.SetString("CodHotel", CodHotel);

                    string purchaseVerification = Sha2.getStringSHA(Constants.acquirerId + Constants.idCommerce + purchaseOperationNumb + purchaseAmountString + Constants.purchaseCurrencyCode + Constants.claveSecreta);
                    return Json(new { IsSuccess = true, purchaseOperationNumber = purchaseOperationNumb, purchaseVerification = purchaseVerification, purchaseAmount = purchaseAmountString, CodigoReserva = responseReservaPrevia.Result.CodReserva });
                }
                else
                {
                    return Json(new { IsSuccess = false });
                }
            }
            catch
            {
                return Json(new { IsSuccess = false });
            }
        }

        [HttpPost]
        public async Task<IActionResult> respuestapago()
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //Informacion Blogs ------------------------------
            var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
            if (responseBlog.Result != null && responseBlog.Result.blog.Count > 0)
            {
                ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);                
            }


            //Datos reales
            
            string claveSecreta = Constants.claveSecreta;
            string purchaseVerificationPayMe = HttpContext.Request.Form["purchaseVerification"]; // ****
            var HttpAcquirerId = HttpContext.Request.Form["acquirerId"];
            var HttpIcommerce = HttpContext.Request.Form["idCommerce"];
            var HttpPurchaseOperationNumber = HttpContext.Request.Form["purchaseOperationNumber"];
            var HttpPurchaseAmount = HttpContext.Request.Form["purchaseAmount"];
            var HttppurchaseCurrencyCode = HttpContext.Request.Form["purchaseCurrencyCode"];
            var HttpAuthorizationResult = HttpContext.Request.Form["authorizationResult"];            
            var CodHotel = HttpContext.Request.Form["reserved2"];
            var CodReserva = HttpContext.Request.Form["reserved3"];


            ViewBag.purchaseVerificationPayMe = purchaseVerificationPayMe;
            ViewBag.HttpAcquirerId = HttpAcquirerId;
            ViewBag.HttpIcommerce = HttpIcommerce;
            ViewBag.HttpPurchaseOperationNumber = HttpPurchaseOperationNumber;
            ViewBag.HttpPurchaseAmount = HttpPurchaseAmount;
            ViewBag.HttppurchaseCurrencyCode = HttppurchaseCurrencyCode;
            ViewBag.CodHotel = CodHotel;
            ViewBag.CodReserva = CodReserva;
            

            ViewBag.language = HttpContext.Request.Form["reserved4"];
            ViewBag.shippingFirstName = HttpContext.Request.Form["shippingFirstName"];
            ViewBag.shippingLastName = HttpContext.Request.Form["shippingLastName"];
            ViewBag.shippingEmail = HttpContext.Request.Form["shippingEmail"];
            ViewBag.shippingAddress = HttpContext.Request.Form["shippingAddress"];
            ViewBag.shippingZIP = HttpContext.Request.Form["shippingZIP"];
            ViewBag.shippingCity = HttpContext.Request.Form["shippingCity"];
            ViewBag.shippingState = HttpContext.Request.Form["shippingState"];
            ViewBag.shippingCountry = HttpContext.Request.Form["shippingCountry"];
            ViewBag.descriptionProducts = HttpContext.Request.Form["descriptionProducts"];
            ViewBag.programmingLanguage = HttpContext.Request.Form["reserved5"];
            ViewBag.billingFirstName = HttpContext.Request.Form["billingFirstName"];
            ViewBag.billingLastName = HttpContext.Request.Form["billingLastName"];
            ViewBag.billingEmail = HttpContext.Request.Form["billingEmail"];
            ViewBag.billingAddress = HttpContext.Request.Form["billingAddress"];
            ViewBag.billingZip = HttpContext.Request.Form["reserved6"];
            ViewBag.billingCity = HttpContext.Request.Form["billingCity"];

            ViewBag.CodTipoHab = HttpContext.Request.Form["reserved7"];
            ViewBag.FechaReserva = HttpContext.Request.Form["reserved8"];
            ViewBag.FechaLlegada = HttpContext.Request.Form["reserved9"];
            ViewBag.FechaSalida = HttpContext.Request.Form["reserved10"];
            ViewBag.NumNoches = HttpContext.Request.Form["reserved11"];
            ViewBag.PrecioNoche = HttpContext.Request.Form["reserved12"];
            ViewBag.Telefonos = HttpContext.Request.Form["reserved13"];
            ViewBag.Comentarios = HttpContext.Request.Form["reserved14"];
            ViewBag.Adultos = HttpContext.Request.Form["reserved15"];
            ViewBag.Ninos = HttpContext.Request.Form["reserved16"];
            ViewBag.MontoTotalReserva = HttpContext.Request.Form["reserved17"];




            ViewBag.VolverPagar = false;

            //DatosFicticios
            /*
            string claveSecreta = Constants.claveSecreta;
            string purchaseVerificationPayMe = "d68928f597e45b7613c92b797d24cc3febf12749661251ca6f7306f728c91f322db84d3519f4fa68a5afef663d9ff59f8d6f1367ab4f7720706edd2d8107861c"; // ****
            var HttpAcquirerId = Constants.acquirerId;
            var HttpIcommerce = Constants.idCommerce;
            var HttpPurchaseOperationNumber = "000000589";
            var HttpPurchaseAmount = "37.5";
            var HttppurchaseCurrencyCode = Constants.purchaseCurrencyCode;
            var HttpAuthorizationResult = "asdfasdfasdfasdflasdjfldjasf";
            var CodHotel = "105010001";
            var CodReserva = "10be7fcc";

            CodHotel = HttpContext.Session.GetString("CodHotel");
            CodReserva = HttpContext.Session.GetString("CodReserva"); */

            logger.LogInformation("purchaseVerificationPayMe:" + purchaseVerificationPayMe);
            logger.LogInformation("HttpPurchaseOperationNumber:" + HttpPurchaseOperationNumber);
            logger.LogInformation("HttpPurchaseAmount:" + HttpPurchaseAmount);
            logger.LogInformation("HttpAuthorizationResult:" + HttpAuthorizationResult);
            logger.LogInformation("CodHotel:" + CodHotel);
            logger.LogInformation("CodReserva:" + CodReserva);

            logger.LogInformation("language:" + HttpContext.Request.Form["reserved4"]);
            logger.LogInformation("shippingFirstName:" + HttpContext.Request.Form["shippingFirstName"]);
            logger.LogInformation("shippingLastName:" + HttpContext.Request.Form["shippingLastName"]);
            logger.LogInformation("shippingEmail:" + HttpContext.Request.Form["shippingEmail"]);
            logger.LogInformation("shippingAddress:" + HttpContext.Request.Form["shippingAddress"]);
            logger.LogInformation("shippingZIP:" + HttpContext.Request.Form["shippingZIP"]);
            logger.LogInformation("shippingCity:" + HttpContext.Request.Form["shippingZIP"]);
            logger.LogInformation("shippingState:" + HttpContext.Request.Form["shippingState"]);
            logger.LogInformation("shippingCountry:" + HttpContext.Request.Form["shippingCountry"]);
            logger.LogInformation("descriptionProducts:" + HttpContext.Request.Form["descriptionProducts"]);
            logger.LogInformation("programmingLanguage:" + HttpContext.Request.Form["reserved5"]);
            logger.LogInformation("billingFirstName:" + HttpContext.Request.Form["billingFirstName"]);
            logger.LogInformation("billingLastName:" + HttpContext.Request.Form["billingLastName"]);
            logger.LogInformation("billingEmail:" + HttpContext.Request.Form["billingEmail"]);
            logger.LogInformation("billingAddress:" + HttpContext.Request.Form["billingAddress"]);
            logger.LogInformation("billingZip:" + HttpContext.Request.Form["reserved6"]);
            logger.LogInformation("billingCity:" + HttpContext.Request.Form["billingCity"]);


 


            string purchaseVerificationCom = Sha2.getStringSHA(HttpAcquirerId + HttpIcommerce + HttpPurchaseOperationNumber + HttpPurchaseAmount + HttppurchaseCurrencyCode + HttpAuthorizationResult + claveSecreta);

            
            ViewBag.iconResponse = "fal fa-times-circle decsth errorPago";
            ViewBag.TituloRespuesta = "Su pago no se ha podido verificar";
            ViewBag.CuerpoRespuesta = "Su pago no se ha podido verificar";



            if (purchaseVerificationPayMe == purchaseVerificationCom) {
                logger.LogInformation("Respuesta pago (verificacion valida)");
                //Consulta payme
                var purchaseVerificationConsulta = Sha2.getStringSHA(HttpAcquirerId + HttpIcommerce + HttpPurchaseOperationNumber + claveSecreta);
                string DATA = "{\"idAcquirer\":\"" + HttpAcquirerId + "\",\"idCommerce\":\"" + HttpIcommerce + "\",\"operationNumber\":\"" + HttpPurchaseOperationNumber + "\",\"purchaseVerification\":\"" + purchaseVerificationConsulta + "\"}";


                try
                {
                    var responsePagoPayme = await _apiService.ConsultaPayMe("https://integracion.alignetsac.com", "VPOS2", "/rest/operationAcquirer/consulte", DATA);
                    if (responsePagoPayme.IsSuccess)
                    {
                        logger.LogInformation("Respuesta pago (consulta pago exitoso)");

                        if (int.Parse(responsePagoPayme.Result.result) == 3)
                        {
                            logger.LogInformation("Respuesta pago (pago autorizado)");
                            ViewBag.iconResponse = "fal fa-check-circle decsth";
                            ViewBag.TituloRespuesta = "Gracias. Su reserva ha sido recibida";
                            ViewBag.CuerpoRespuesta = responsePagoPayme.Result.errorMessage;

                            //Envio resultados

                            EncriptadorAEScs Seguridad = new EncriptadorAEScs();
                            var OrdenRefenciaEncriptado = Seguridad.cifrarTextoAES(HttpPurchaseOperationNumber, fraseEncriptacion, valorRGBSalt, algoritmoEncriptacionHASH, iteraciones, vectorInicial, tamanoClave);
                            var requestPayme = new RequestPaymeUpdate()
                            {
                                OrderReference = OrdenRefenciaEncriptado,
                                CodHotel = CodHotel,
                                CodReserva = CodReserva,
                                CodRespuesta = responsePagoPayme.Result.errorCode.Trim(),
                                Fecha = DateTime.Now.ToString("yyyy-MM-dd"),
                                Hora = DateTime.Now.ToString("HH:mm:ss"),
                                MensajeRespuesta = responsePagoPayme.Result.errorMessage,
                                MTI = "",
                                NumeroAutorizacion = responsePagoPayme.Result.authorizationCode,
                                NumeroReferencia = responsePagoPayme.Result.operationNumber
                            };
                            logger.LogInformation("Respuesta pago (json):" + JsonConvert.SerializeObject(requestPayme));                            
                            if (!string.IsNullOrEmpty(CodReserva))
                            {
                                var responseReservaPrevia = await _apiService.UpdatePaymeCheck(url, Constants.UrlAPP, "/api/CCReservas/Pagar", _dataAccess.token_type, _dataAccess.access_token, requestPayme);
                                if (responseReservaPrevia.IsSuccess)
                                {
                                    logger.LogInformation("Respuesta pago (api exitoso)");
                                    ViewBag.CuerpoRespuesta = "Su reserva se ha registrado correctamente";
                                    HttpContext.Session.SetString("CodHotel", "");
                                    HttpContext.Session.SetString("CodReserva", "");
                                }
                                else
                                {
                                    logger.LogInformation("Respuesta pago (api NO exitoso)");
                                    ViewBag.CuerpoRespuesta = "Su reserva no se ha registrado en el sistema";
                                }
                            }

                        }
                        else
                        {
                            logger.LogInformation("Respuesta pago (pago no autorizado)");
                            ViewBag.iconResponse = "fal fa-times-circle decsth errorPago";
                            ViewBag.TituloRespuesta = "No se puedo realizar el pago correctamente";
                            ViewBag.CuerpoRespuesta = responsePagoPayme.Result.errorMessage;
                            ViewBag.VolverPagar = true;
                        }

                        logger.LogInformation("Respuesta pago (Enviare datos al api costarica)");
                        
                        
                    }
                    else
                    {
                        logger.LogInformation("Respuesta pago (consula pago no exitoso)");
                        ViewBag.iconResponse = "fal fa-times-circle decsth errorPago";
                        ViewBag.TituloRespuesta = "No se puedo verificar el pago";
                        ViewBag.CuerpoRespuesta = "No se puedo consultar la informacion del pago en el banco";
                        //return Json(new { IsSuccess = false });
                    }
                }
                catch
                {
                    logger.LogInformation("Algo fallo)");
                }
            }
            else
            {
                HttpContext.Session.SetString("CodHotel", "");
                HttpContext.Session.SetString("CodReserva", "");
                ViewBag.CuerpoRespuesta = "Transacción Invalida. Los datos fueron alterados en el proceso de respuesta";
                logger.LogInformation("Respuesta pago (Datos no validos del banco)");
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> BusquedaHotelesDisponibilidad(string codHotel, int cantAdultos, int cantNinos, string fechaInicio, string fechaFinal, string CodTipoHab)
        {
            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;


            DateTime DtFechaini;
            DateTime DtFechaFin;

            var arrayFechaIni = fechaInicio.Split("-");
            var arrayFechaFin = fechaFinal.Split("-");
            fechaInicio = arrayFechaIni[2] + "/" + arrayFechaIni[1] + "/" + arrayFechaIni[0];
            fechaFinal = arrayFechaFin[2] + "/" + arrayFechaFin[1] + "/" + arrayFechaFin[0];

            double TotalHabitacion = 0;
            ViewBag.TotalHabitacion = TotalHabitacion;
            if (DateTime.TryParse(fechaInicio, out DtFechaini) && DateTime.TryParse(fechaFinal, out DtFechaFin))
            {

                //informacion de reservacion
                try
                {
                    //Verificacion Disponibilidad
                    HabitacionRequest habRequest = new HabitacionRequest
                    {
                        codHotel = codHotel,
                        cantAdultos = cantAdultos,
                        cantNinos = cantNinos,
                        fechaInicio = DtFechaini.ToString("yyyy-MM-dd"),
                        fechaFinal = DtFechaFin.ToString("yyyy-MM-dd")
                    };

                    var responseHabitacionesDisponibles = await _apiService.GetHabitacionesDisponibles(url, Constants.UrlAPP, "/api/CCHabitacionesTipos/GetDisponibilidad", _dataAccess.token_type, _dataAccess.access_token, habRequest);
                    if (responseHabitacionesDisponibles.IsSuccess)
                    {
                        if (responseHabitacionesDisponibles.Result.Count > 0)
                        {
                            var HabDisponible = responseHabitacionesDisponibles.Result.Where(x => x.CodTipoHab == CodTipoHab).FirstOrDefault();
                            if (HabDisponible != null)
                            {
                                TotalHabitacion = HabDisponible.Precio;
                                return Json(new { IsSuccess = true, TotalHabitacion = "$" + TotalHabitacion.ToString("F") });

                            }
                        }

                        return Json(new { IsSuccess = false, mensaje = "Habitacion no disponible" });
                    }
                    else
                    {
                        return Json(new { IsSuccess = false });
                    }
                }
                catch
                {
                    return Json(new { IsSuccess = false });
                }

            }
            else
            {
                return Json(new { IsSuccess = false });
            }
        }

    }
}