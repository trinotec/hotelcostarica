﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HotelCostaRicaCore.Web.Models;
using HotelCostaRicaCore.Web.Services;
using HotelCostaRicaCore.Web.Helpers;
using HotelCostaRicaCore.Web.ViewModels;
using System.IO;
using Newtonsoft.Json;

namespace HotelCostaRicaCore.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IApiService _apiService;


        public HomeController(ILogger<HomeController> logger, IApiService apiService)
        {
            _logger = logger;
            this._apiService = apiService;
        }

        public async Task<IActionResult> Index()
        { 
            try
            {

                //informacion de acceso ------------------------------
                var url = Constants.UrlAPI;
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;

         
                //Informacion Hoteles
                var responseHoteles = await _apiService.GetHoteles(url, Constants.UrlAPP, "/api/Hoteles", _dataAccess.token_type, _dataAccess.access_token);                 

                if (responseHoteles.IsSuccess)
                {
                    var hoteles = responseHoteles.Result;

                    ViewBag.HotelesExplora = hoteles.Take(8);
                    ViewBag.HotelesPopulares = hoteles.Where(x => x.Popular == true).ToList();
                    ViewBag.HotelesRecientes = hoteles.Where(x => x.Reciente == true).ToList();

                }

                //Informacion Testimonials 
                var responseTestimonials = await _apiService.GetTestimonials(url, Constants.UrlAPP, "/api/CCTestimonial", _dataAccess.token_type, _dataAccess.access_token);
                if (responseTestimonials.IsSuccess)
                {                    
                    ViewBag.Testimonials = responseTestimonials.Result;
                }

                //Informacion Blogs ------------------------------
                var responseBlog = await _apiService.GetBlogs(url, Constants.UrlAPP, "/api/Blog", _dataAccess.token_type, _dataAccess.access_token);
                if (responseBlog.Result != null &&  responseBlog.Result.blog.Count > 0)
                {
                    ViewBag.BlogsFooter = responseBlog.Result.blog.Skip(0).Take(3);
                }

            }
            catch (Exception ex)
            {
                //Accion error Consulta WebApi
                //var msg = ex.Message; 
            }


            var reader = new StreamReader(Directory.GetCurrentDirectory() + "/appsettings.json");
            var appSettings = JsonConvert.DeserializeObject<AppSettingsProy>(reader.ReadToEnd());
            ViewBag.Articulos = appSettings.Articulos.Take(3);
            ViewBag.ArticulosFooter= appSettings.Articulos.Take(3);

            return await Task.Run(() => View());

        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
