﻿using HotelCostaRicaCore.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Services
{
    public class ApiService : IApiService
    {
         
        public async Task<Response<Access>> GetAccess(string urlBase, string servicePrefix, string controller, AccessRequest request)
        {
            try
            {

                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(
                    string.Format("grant_type=password&username={0}&password={1}", request.UserName, request.Password),
                    Encoding.UTF8,
                    "application/x-www-form-urlencoded"
                    );

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";

                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<Access>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result),
                    };
                }

                var ResultAccess = JsonConvert.DeserializeObject<Access>(result);
                return new Response<Access>
                {
                    IsSuccess = true,
                    Result = ResultAccess
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<Access>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }


        public async Task<Response<List<Hotel>>> GetHoteles(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<Hotel>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<Hotel>>(result);
                return new Response<List<Hotel>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<Hotel>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<Hotel>> GetHotelDetalle(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<Hotel>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<Hotel>(result);
                return new Response<Hotel>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<Hotel>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        public async Task<Response<List<Testimonial>>> GetTestimonials(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<Testimonial>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<Testimonial>>(result);
                return new Response<List<Testimonial>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<Testimonial>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        public async Task<Response<List<HotelFoto>>> GetHotelesFotos(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<HotelFoto>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<HotelFoto>>(result);
                return new Response<List<HotelFoto>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<HotelFoto>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }



        public async Task<Response<List<Habitacion>>> GetHabitacionesDisponibles(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, HabitacionRequest request)
        {
            try
            {
                //var data = new EvaluacionRequest { estatus = 2 };
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("codHotel={0}&cantAdultos={1}&cantNinos={2}&fechaInicio={3}&fechaFinal={4}", request.codHotel, request.cantAdultos, request.cantNinos, request.fechaInicio, request.fechaFinal);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<Habitacion>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<Habitacion>>(result);
                return new Response<List<Habitacion>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<Habitacion>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        public async Task<Response<List<HabitacionFoto>>> GetHabitacionFotos(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, HabitacionFotoRequest request)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}/{request.CodHotel}/{request.CodTipoHab}";
                UriBuilder builder = new UriBuilder(url);
                //builder.Query = string.Format("CodHotel={0}&CodTipoHab={1}", request.CodHotel, request.CodTipoHab);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<HabitacionFoto>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<HabitacionFoto>>(result);
                return new Response<List<HabitacionFoto>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<HabitacionFoto>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<CalificacionHotel>> GetCalificacionHotel(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<CalificacionHotel>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<CalificacionHotel>(result);
                return new Response<CalificacionHotel>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<CalificacionHotel>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<List<Habitacion>>> GetHabitacionDetalle(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, string codHotel, string codHabitacion)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);
                var url = $"{urlBase}/{servicePrefix}{controller}/{codHotel}/{codHabitacion}";
                UriBuilder builder = new UriBuilder(url);                

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<Habitacion>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<Habitacion>>(result);
                return new Response<List<Habitacion>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<Habitacion>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        public async Task<Response<List<Habitacion>>> GetHabitacionesHotel(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<Habitacion>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<Habitacion>>(result);
                return new Response<List<Habitacion>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<Habitacion>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<List<Hotel>>> GetHotelesFiltro(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, string Parametros)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}{Parametros}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<Hotel>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<Hotel>>(result);
                return new Response<List<Hotel>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<Hotel>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }






        public async Task<Response<ResponseBlog>> GetBlogs(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<ResponseBlog>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<ResponseBlog>(result);
                return new Response<ResponseBlog>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<ResponseBlog>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }



        public async Task<Response<ResponseDetalleBlog>> GetBlogDetalle(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<ResponseDetalleBlog>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<ResponseDetalleBlog>(result);
                return new Response<ResponseDetalleBlog>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<ResponseDetalleBlog>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        
        public async Task<Response<Blogger>> PostGuardarComentarioBlog(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, RequestComentarioBlog request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<Blogger>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<Blogger>(result);
                return new Response<Blogger>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<Blogger>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        public async Task<Response<ResponseReservacion>> PostInsertReservation(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, string requestString)
        {
            try
            {
                //var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<ResponseReservacion>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<ResponseReservacion>(result);
                return new Response<ResponseReservacion>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<ResponseReservacion>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<ResponsePagoReserva>> UpdatePaymeCheck(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, RequestPaymeUpdate request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<ResponsePagoReserva>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var res = JsonConvert.DeserializeObject<ResponsePagoReserva>(result);
                return new Response<ResponsePagoReserva>
                {
                    IsSuccess = true,
                    Result = res
                };
            }
            catch (Exception ex)
            {
                return new Response<ResponsePagoReserva>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<ResponsePayme>> ConsultaPayMe(string urlBase, string servicePrefix, string controller, string requestString)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase),                      
                };
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                
                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.PostAsync(builder.Uri, content);
                var result = await response.Content.ReadAsStringAsync();

                
                if (!response.IsSuccessStatusCode)
                {
                    return new Response<ResponsePayme>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<ResponsePayme>(result);
                return new Response<ResponsePayme>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<ResponsePayme>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

         
    }
}
