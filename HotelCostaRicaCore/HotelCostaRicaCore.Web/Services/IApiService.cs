﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelCostaRicaCore.Web.Models;

namespace HotelCostaRicaCore.Web.Services
{
    public interface IApiService
    { 
        Task<Response<Access>> GetAccess(
          string urlBase,
          string servicePrefix,
          string controller,
          AccessRequest request
      );

        Task<Response<List<Hotel>>> GetHoteles(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken
        );

        Task<Response<Hotel>> GetHotelDetalle(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken
        );

        Task<Response<List<Testimonial>>> GetTestimonials(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken
        );

        Task<Response<List<HotelFoto>>> GetHotelesFotos(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken
        );

        Task<Response<List<Habitacion>>> GetHabitacionesDisponibles(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            HabitacionRequest request
        );

        Task<Response<List<HabitacionFoto>>> GetHabitacionFotos(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            HabitacionFotoRequest request
        );

        Task<Response<CalificacionHotel>> GetCalificacionHotel(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken
        );

        Task<Response<List<Habitacion>>> GetHabitacionDetalle(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken, 
            string codHotel, 
            string codHabitacion
        );

        Task<Response<List<Habitacion>>> GetHabitacionesHotel(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken            
        );

        Task<Response<List<Hotel>>> GetHotelesFiltro(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            string Parametros
        );


        Task<Response<ResponseBlog>> GetBlogs(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken
        );

        Task<Response<ResponseDetalleBlog>> GetBlogDetalle(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken
        );


        Task<Response<Blogger>> PostGuardarComentarioBlog(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            RequestComentarioBlog request
        );

        Task<Response<ResponseReservacion>> PostInsertReservation(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            string requestString
        );

        Task<Response<ResponsePagoReserva>> UpdatePaymeCheck(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            RequestPaymeUpdate request
        );
        Task<Response<ResponsePayme>> ConsultaPayMe(
            string urlBase,
            string servicePrefix,
            string controller,            
            string Content
        );

    }
}
