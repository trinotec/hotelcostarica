﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class HotelesServicio
    {
        public string CodServicio { get; set; }
        public string NomServicio { get; set; }
        public object Icono { get; set; }
        public string IconoFullPath { get; set; }
        public object IconoBase64 { get; set; }
    }
}
