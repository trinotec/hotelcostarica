﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class ResponseReservacion
    {
        public string CodReserva { get; set; }
        public int orderReference { get; set; }
    }
}
