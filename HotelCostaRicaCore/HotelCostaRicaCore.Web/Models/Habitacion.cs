﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class Habitacion
    {
        public List<Amenidad> Amenidades { get; set; }
        public List<Servicio> Servicios { get; set; }
        public string CodHotel { get; set; }
        public string CodTipoHab { get; set; }
        public string NombreTipoHab { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionDetallada { get; set; }
        public string Camas { get; set; }
        public int CapacidadAdultos { get; set; }
        public int CapacidadNinos { get; set; }
        public int CapacidadMin { get; set; }
        public int MinimoNoches { get; set; }
        public int TotalHabitaciones { get; set; }
        public double Precio { get; set; }
        public List<HabitacionFoto>Fotos { get; set; }
        public string  FotoPrincipal { get; set; }
        public string FotosCadena { get; set; }
                
        public int CapacidadMax { get; set; }        
        public int CantidadBanos { get; set; }
        public int TamanoHabitacion { get; set; }
        public double CostoExtraAdul { get; set; }
        public double CostoExtraNin { get; set; }

    }
}
