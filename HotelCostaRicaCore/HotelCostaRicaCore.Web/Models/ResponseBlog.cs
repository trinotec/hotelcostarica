﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class ResponseBlog
    {
        public List<Blog> blog { get; set; }
        public Legal legal { get; set; }
    }
}
