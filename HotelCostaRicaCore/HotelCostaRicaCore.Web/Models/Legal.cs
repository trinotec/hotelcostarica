﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class Legal
    {
        public string TerminosYCondiciones { get; set; }
        public string PoliticasCancelacion { get; set; }
        public string PoliticasPrivacidad { get; set; }
        public int TipoBanco { get; set; }
    }
}
