﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class Reservacion
    {
        public string CodHotel { get; set; }
        public string CodTipoHab { get; set; }
        public string FechaReserva { get; set; }
        public string FechaLlegada { get; set; }
        public string FechaSalida { get; set; }
        public int NumNoches { get; set; }
        public double PrecioNoche { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Telefonos { get; set; }
        public string NumIdentificacion { get; set; }
        public string CodPais { get; set; }
        public string CodCiudad { get; set; }
        public string Email { get; set; }
        public string Empresa { get; set; }
        public string Comentarios { get; set; }
        public int Adultos { get; set; }
        public int Ninos { get; set; }
        public double MontoTotalReserva { get; set; }
        public double MontoPagado { get; set; }
        public double PagoTarjeta { get; set; }
 
       
    }
}
