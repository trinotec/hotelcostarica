﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class Hotel
    {
        public List<HotelesAmenidad> HotelesAmenidades { get; set; }
        public List<HotelesServicio> HotelesServicios { get; set; }
        public string CodHotel { get; set; }
        public string Nombre { get; set; }
        public string DescripcionCorta { get; set; }
        public string DescripcionLarga { get; set; }
        public string UrlVideo { get; set; }
        public string Direccion { get; set; }
        public string Ubicacion1 { get; set; }
        public string Ubicacion2 { get; set; }
        public string PalabrasClaves { get; set; }
        public double PrecioPromedio { get; set; }
        public int Estrellas { get; set; }
        public double Calificacion { get; set; }
        public int CantidadCalificaciones { get; set; }
        public double PorcentajeDescuento { get; set; }
        public bool Reciente { get; set; }
        public bool Popular { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Banner { get; set; }
        public string BannerFullPath { get; set; }
        public string Leyenda { get; set; }
        public DateTime FechaRegistro { get; set; }
        public double PrecioMaximo { get; set; }
        public double PrecioMinimo { get; set; }
        public int NumLocalizacionMapa { get; set; }

        public string Descripcion1 { get; set; }
        public string Descripcion2 { get; set; }
        public string Descripcion3 { get; set; }

        public string DesayunoIncluido { 
            get {
                if (HotelesServicios.Where(x => x.NomServicio.ToLower().IndexOf("desayuno incluido") >= 0).Count() <= 0)
                {
                    return "none";
                }
                return "";
            } 
        }
    }
}
