﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class HabitacionFotoRequest
    {
        public string CodHotel { get; set; }
        public string CodTipoHab { get; set; }
    }
}
