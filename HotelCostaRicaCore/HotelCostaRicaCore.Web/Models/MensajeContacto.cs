﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class MensajeContacto
    {
        public string NomCliente { get; set; }
        public string Email { get; set; }        
        public string Mensaje { get; set; }        
    }
}
