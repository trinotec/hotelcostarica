﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class ResponseDetalleBlog
    {
        public Blog blog { get; set; }
        public IList<Blogger> Bloggers { get; set; }

    }
}
