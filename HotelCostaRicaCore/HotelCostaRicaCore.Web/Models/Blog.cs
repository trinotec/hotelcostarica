﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class Blog
    {
        public int idBlog { get; set; }
        public string Imagen { get; set; }
        public DateTime Fecha { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Usuario { get; set; }
        public int Vistas { get; set; }
        public string Tags { get; set; }
        public string ImageFullPath { get; set; }
        public object ImageBase64 { get; set; }

        public List<string> ListTags { 
            get {
                var arrayTags = new List<string>(Tags.Split(","));
                return arrayTags;
            }  
        }

    }
}
