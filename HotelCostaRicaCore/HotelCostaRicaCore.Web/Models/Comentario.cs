﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class ComentarioHotel
    {
        public int id { get; set; }
        public string Nombre { get; set; }
        public string URLFoto { get; set; }
        public string URLFotoFullPath { get; set; }
        public string CodHotel { get; set; }
        public string CodReserva { get; set; }
        public DateTime Fecha { get; set; }
        public string Comentario { get; set; }
        public double Calificacion { get; set; }
        public string Leyenda { get; set; }
        public int Calificacion1 { get; set; }
        public int Calificacion2 { get; set; }
        public int Calificacion3 { get; set; }
        public int Calificacion4 { get; set; }
    }
}
