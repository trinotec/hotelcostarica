﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class ReservacionCodReservaAct : Reservacion
    {
        public string CodReserva { get; set; }
    }
}
