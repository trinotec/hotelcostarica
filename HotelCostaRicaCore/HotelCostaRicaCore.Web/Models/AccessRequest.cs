﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class AccessRequest
    {
        public string Grant_type { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
