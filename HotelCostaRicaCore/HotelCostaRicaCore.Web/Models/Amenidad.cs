﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class Amenidad
    {
        public string CodAmenidad { get; set; }
        public string NomAmenidad { get; set; }
    }
}
