﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class RequestComentarioBlog
    {
        public int idBlog { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Comentario { get; set; }
    }
}
