﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class ResponsePayme
    {
        public string authenticationECI { get; set; }
        public string authorizationCode { get; set; }
        public string billingAddress { get; set; }
        public string billingCity { get; set; }
        public string billingCountry { get; set; }
        public string billingEMail { get; set; }
        public string billingFirstName { get; set; }
        public string billingLastName { get; set; }
        public string billingState { get; set; }
        public string cardNumber { get; set; }
        public string cardType { get; set; }
        public string errorCode { get; set; }
        public string errorMessage { get; set; }
        public string language { get; set; }
        public string operationNumber { get; set; }
        public string purchaseAmount { get; set; }
        public string purchaseCurrencyCode { get; set; }
        public string purchaseIPAddress { get; set; }
        public string result { get; set; }
        public string shippingAddress { get; set; }
        public string shippingCity { get; set; }
        public string shippingCountry { get; set; }
        public string shippingEMail { get; set; }
        public string shippingFirstName { get; set; }
        public string shippingLastName { get; set; }
        public string shippingState { get; set; }
        public string shippingZIP { get; set; }
        public string terminalCode { get; set; }
    }
}
