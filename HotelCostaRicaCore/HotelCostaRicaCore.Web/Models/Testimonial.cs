﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class Testimonial
    {
        public int id { get; set; }
        public string CodReserva { get; set; }
        public DateTime Fecha { get; set; }
        public string Nombre { get; set; }
        public string Comentario { get; set; }
        public double Calificacion { get; set; }
        public string UrlFoto { get; set; }
        public string UrlFotoFullPath { get; set; }
        public object UrlFotoB64 { get; set; }
    }
}
