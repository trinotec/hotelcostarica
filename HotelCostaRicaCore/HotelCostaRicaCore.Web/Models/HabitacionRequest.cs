﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class HabitacionRequest
    {
        public string codHotel { get; set; }
        public int cantAdultos { get; set; }
        public int cantNinos { get; set; }
        public string fechaInicio { get; set; }
        public string fechaFinal { get; set; }
    }
}
