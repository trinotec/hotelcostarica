﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class Blogger
    {
        public int id { get; set; }
        public int idBlog { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Comentario { get; set; }
        public DateTime Fecha { get; set; }

        public string FechaString { get {

                return Fecha.ToString("dd MMMM yyyy");
            }        
        }

    }
}
