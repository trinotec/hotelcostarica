﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class CalificacionHotel
    {
        public double Calificacion { get; set; }
        public double Limpieza { get; set; }
        public double Comfort { get; set; }
        public double Staf { get; set; }
        public double Facilidades { get; set; }
        public double TotalComentarios { get; set; }
        public string Leyenda { get; set; }
        public List<ComentarioHotel> comentario { get; set; }
    }
}
