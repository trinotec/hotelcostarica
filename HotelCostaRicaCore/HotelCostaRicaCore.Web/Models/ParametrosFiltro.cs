﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class ParametrosFiltro
    {
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }        
        public string Provincia { get; set; }
        public string Textobusqueda { get; set; }
        public int CantidadHabitaciones { get; set; }
        public int TotalAdultos { get; set; }
        public int TotalNinos { get; set; }
        public int PrecioMinimo { get; set; }
        public int PrecioMax { get; set; }
        public int Orden { get; set; }
        public bool CheckEstrella5 { get; set; }
        public bool CheckEstrella4 { get; set; }
        public bool CheckEstrella3 { get; set; }
        public bool CheckAire { get; set; }
        public bool Checkdesayuno { get; set; }
        public bool Checkpiscina     { get; set; }
        public bool CheckWifi { get; set; }
        public bool CheckParqueo { get; set; }
        public bool CheckDivisas { get; set; }
 
	}
}
