﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class HotelFoto
    {
        public int id { get; set; }
        public string CodHotel { get; set; }
        public string UrlFoto { get; set; }       
    }
}
