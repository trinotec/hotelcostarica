﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class RequestPaymeUpdate
    {
        public string OrderReference { get; set; }
        public string CodReserva { get; set; }
        public string CodHotel { get; set; }
        public string CodRespuesta { get; set; }
        public string MensajeRespuesta { get; set; }
        public string NumeroAutorizacion { get; set; }
        public string NumeroReferencia { get; set; }
        public string MTI { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
    }
}
