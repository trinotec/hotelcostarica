﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class ResponsePagoReserva:Reservacion
    {
        public string CodReserva { get; set; }
        public string StatusReserva { get; set; }
        public double PagoEfectivo { get; set; }
        public double PagoTransferencia { get; set; }
        public object NumTransferencia { get; set; }
        public object MotivoCancelacion { get; set; }
        public object MotivoFecha { get; set; }
        public object NumeroComprobantePago { get; set; }
    }
}
