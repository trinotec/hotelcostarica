﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.Models
{
    public class Articulo
    {
        public int idBlog { get; set; }
        public string Titulo { get; set; }
        public string TituloCorto { get; set; }
        public string Contenido { get; set; }
        public string ContenidoCorto { get; set; }
        public string ContenidoHome { get; set; }
        public string DirImage { get; set; }
        public int Vistas { get; set; }
        public string FechaArticulo { get; set; } 
    }
}
