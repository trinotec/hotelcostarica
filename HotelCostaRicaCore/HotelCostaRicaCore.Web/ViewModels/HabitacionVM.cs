﻿using HotelCostaRicaCore.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.ViewModels
{
    public class HabitacionVM : Habitacion
    {       
        public List<HabitacionFoto> Fotos  { get; set; }
    }
}
