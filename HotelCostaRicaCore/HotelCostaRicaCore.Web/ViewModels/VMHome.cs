﻿using HotelCostaRicaCore.Web.Helpers;
using HotelCostaRicaCore.Web.Models;
using HotelCostaRicaCore.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCostaRicaCore.Web.ViewModels
{
    public class VMHome
    {
        private readonly IApiService _apiService;
        private readonly Access _dataAccess;
        private readonly string  url;
        
        public IEnumerable<Hotel> Hoteles{ get; set; }
        public IEnumerable<Hotel> DestinosPopulares{ 
            get {
                IEnumerable<Hotel> DP;
                if (Hoteles != null)
                {
                    DP = Hoteles.Skip(0).Take(8);                    
                }
                else
                {
                    DP = null;
                }
                return DP;                
            } 
        }

        public string prueba { get; set; }

        public VMHome(){}

        public VMHome(IApiService apiService)
        {
            this._apiService = apiService;
            url = Constants.UrlAPI;
            var  acceso = GetAccess.Instance(_apiService).GetToken();
             _dataAccess = acceso.Result;
            var Hotelesyy = BuscarHoteles();
            prueba = "Rafael";
        }


        public async Task<IEnumerable<Hotel>> BuscarHoteles()
        {
            IEnumerable<Hotel> HotelesX = new List<Hotel>();
            var responseAccess = await _apiService.GetHoteles(url, Constants.UrlAPP, "/api/Hoteles", _dataAccess.token_type, _dataAccess.access_token);
            if (!responseAccess.IsSuccess)
            {
                return null;
            }
            HotelesX = responseAccess.Result;
            return await Task.FromResult(HotelesX);
        }

    }
}
