﻿using HotelCostaRicaCore.Web.Helpers;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace HotelCostaRicaCore.Web
{
    public class Constants
    {
        private static IConfigurationRoot Configuration { get; } =
         ConfigurationHelper.GetConfiguration(Directory.GetCurrentDirectory());

        public static string UrlAPI { get; } = Configuration.GetSection("ApiSettings")["UrlApi"];
        public static string UrlAPP { get; } = Configuration.GetSection("ApiSettings")["UrlApp"];        
        public const string Grant_type = "password";
        public const string UserName = "soporte04@sicsoftsa.com";
        public const string Password = "Hola123";

        //Opciones PayMe
        public static string acquirerId { get; } = Configuration.GetSection("PamyMe")["acquirerId"];
        public static string idCommerce { get; } = Configuration.GetSection("PamyMe")["idCommerce"];
        public static string purchaseCurrencyCode { get; } = Configuration.GetSection("PamyMe")["purchaseCurrencyCode"];
        public static string language { get; } = Configuration.GetSection("PamyMe")["language"];
        public static string shippingFirstName { get; } = Configuration.GetSection("PamyMe")["shippingFirstName"];
        public static string shippingLastName { get; } = Configuration.GetSection("PamyMe")["shippingLastName"];
        public static string shippingEmail { get; } = Configuration.GetSection("PamyMe")["shippingEmail"];
        public static string shippingAddress { get; } = Configuration.GetSection("PamyMe")["shippingAddress"];
        public static string shippingZIP { get; } = Configuration.GetSection("PamyMe")["shippingZIP"];
        public static string shippingCity { get; } = Configuration.GetSection("PamyMe")["shippingCity"];
        public static string shippingState { get; } = Configuration.GetSection("PamyMe")["shippingState"];
        public static string shippingCountry { get; } = Configuration.GetSection("PamyMe")["shippingCountry"];
        public static string programmingLanguage { get; } = Configuration.GetSection("PamyMe")["programmingLanguage"];
        public static string claveSecreta { get; } = Configuration.GetSection("PamyMe")["claveSecreta"];

    }
}
